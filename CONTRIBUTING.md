<br><b>5 Phrases International Students Should Know Before Moving to the UK</b><br/>
Image Link: https://www.pexels.com/photo/hispanic-couple-standing-and-having-quarrel-on-street-6532735/
Students from abroad who go to the United Kingdom to study can find it hard to understand English or some of the used phrases. It can be daunting when everybody else is conversing in a different language. A <a href="https://metro.co.uk/2021/04/08/how-to-avoid-life-comparison-when-the-country-goes-back-to-normal-14365884/">person</a> cannot understand them, and they may feel left out. Many phrases can be learned before a student moves to the UK; below are five examples.
<br><b>1)	Cheers (Thank you)</b><br/>
A student in the UK will most certainly hear the word cheers every time; it means thank you. An example of how it is used is when someone picks up an item that an individual has dropped. The proper response is “cheers!” It is used during a toast, a ritual done when taking a drink as a goodwill expression like in a celebration or wedding.
<br><b>2)	Knackered (Tired)</b><br/>
When a person says they are knackered, they are tired; for example, if one comes home from a long working day, they may say that they are knackered and will go to sleep early. It is slang meaning exhausted; although it may be considered impolite, it is not a rude word. One cannot use it in an academic essay or writing.
<br><b>3)	Init (Isn’t It?)</b><br/>
Init is slang for 'isn’t it,' and commonly used while conversing with close acquaintances. It is usually used at the end of sentences when agreeing with others dramatically. Apart from learning these common expressions, students can <a href="https://edubirdie.com/buy-dissertation">buy dissertation online</a> to ease their course work. However, one should note that the phrase should not be used in a formal setting because it is informal and is used by the young generation. The best application of the word init is:
Person 1: “Wow, it’s so hot outside today.”
Friend: “Init!”
<br><b>4)	Bevvy (Alcohol)</b><br/>
Bevvy is the short form of the word beverage and describes a drink other than water. Nowadays, when one hears the phrase ‘bevvy,’ they understand that it means alcohol. For example, one may go to the local bar with friends for a “bevvy.” The term 'bevvied' is derived here, and it means that one drunk too much alcohol.
<br><b>5)	Soz (Sorry!)</b><br/>
'Soz' is a quick way of saying that one is sorry. Sometimes when someone says 'soz,' he or she does not mean it. It is very informal and silly to be an honest apology. It is used among friends in a sarcastic and joking manner.
<br><b>Conclusion</b><br/>
Hopefully, the above tips can help new students from other countries settle in the UK. Also, one can <a href="https://www.southampton.ac.uk/student-life/community">take</a> some language classes that can give them a push in perfecting the English language. The adaptation period is usually short, and these students generally do well.

